import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagecomponents.MainHeaderComponent;
import pagecomponents.MobileBankComponent;
import pages.MainPage;

public class MobileBankTest {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkNewPage() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseBank();
        new MobileBankComponent(driver)
                .checkNewPageHeader("ABB mobile – Sadə və sürətli");
    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
