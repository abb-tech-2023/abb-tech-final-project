import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagecomponents.CardComponent;
import pagecomponents.MainHeaderComponent;
import pages.MainPage;

public class CardPageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkCardPageTitle() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseCards();
        Assert.assertEquals(driver.getTitle(), "Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları");
    }

    @Test
    public void checkCardHeader() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseCards();
        new CardComponent(driver)
                .chooseTamGencCard()
                .checkPageHeader("TamGənc VISA Debet");
    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
