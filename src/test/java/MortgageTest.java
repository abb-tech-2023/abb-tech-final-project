import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pagecomponents.MainHeaderComponent;
import pagecomponents.MortgageComponent;
import pages.MainPage;

public class MortgageTest {

    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkMortgage() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseInternalMortgage();
        new MortgageComponent(driver)
                .checkNoButtonIsSelected();
    }


    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}


