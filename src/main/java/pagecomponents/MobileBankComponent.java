package pagecomponents;

import java.util.Set;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class MobileBankComponent extends AbsPageComponent {

    @FindBy(css = "#js-hover-link > span:nth-child(1) > a")
    private WebElement abbMobileBank;

    @FindBy(css = "h1")
    private WebElement header;

    public MobileBankComponent(WebDriver driver) {
        super(driver);
    }

    public void clickAbbMobileBank() {
        abbMobileBank.click();
    }

    public void checkNewPageHeader(String header) {
        clickAbbMobileBank();
        String mainWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        for (String childWindow : windowHandles) {
            if (!mainWindowHandle.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
            }
            Assert.assertTrue(this.header.isDisplayed(), "Header is not displayed in the new window");
        }
    }
}

