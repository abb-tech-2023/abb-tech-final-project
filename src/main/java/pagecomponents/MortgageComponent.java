package pagecomponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class MortgageComponent extends AbsPageComponent {

    @FindBy(css = "div.col-lg-4.mb-4.mb-lg-0 label.col.mr-2.form-input.cursor-p.js-form-radio[for=\"from_abb_no\"] span.fs-16.ws-nowrap.text-center.fs-lg-18.fw-400")
    private WebElement inputNo;

    public MortgageComponent(WebDriver driver) {
        super(driver);
    }

    public void checkNoButtonIsSelected() {
        inputNo.click();
        boolean isSelected = inputNo.isSelected();
        Assert.assertTrue(isSelected, "No input radio button is selected");
    }

}

