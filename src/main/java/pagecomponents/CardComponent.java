package pagecomponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.CardPage;

public class CardComponent extends AbsPageComponent {

    @FindBy(css = "div.p-2.h-100p.d-flex.flex-column.align-items-start>a>figure>img[alt='tam-genc']")
    private WebElement tamgenccard;

    public CardComponent(WebDriver driver) {
        super(driver);
    }

    public CardPage chooseTamGencCard() {
        tamgenccard.click();
        return new CardPage(driver);
    }
}
