package pagecomponents;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.CardPage;
import pages.MainPage;
import pages.MortgagePage;

public class MainHeaderComponent extends AbsPageComponent {

    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/kartlar\"]")
    private WebElement cards;

    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"]")
    private WebElement main;

    @FindBy(css = "li>span>a[href=\"https://abb-bank.az/az/ferdi/kreditler\"]")
    private WebElement mortgage;

    public MainHeaderComponent(WebDriver driver) {
        super(driver);
    }

    public CardPage chooseCards() {
        cards.click();
        return new CardPage(driver);
    }

    public MainPage chooseBank() {
        main.click();
        return new MainPage(driver);
    }

    public MortgagePage chooseInternalMortgage() {
        mortgage.click();
        return new MortgagePage(driver);
    }
}
